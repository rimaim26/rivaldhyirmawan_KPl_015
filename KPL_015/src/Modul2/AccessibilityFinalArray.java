/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author rivaldhy Irmwan
 * latihan 
 */
public class AccessibilityFinalArray {
    public static final String[] items = { /*...*/};
    
    private static final String getItem(int index) {
        return items[index];
    }
    
    public static final int getItemCount() {
        return items.length;
    }
    
    //clone the array
    public static final String[] getItem() {
        return items.clone();
    }
    
    //unmodifiable wrappers
    public static final List<String> itemList = Collections.unmodifiableList(Arrays.asList(items));

    public static void main(String[] args) {
        //main method
    }
}    
