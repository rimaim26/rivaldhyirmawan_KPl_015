
package Modul2;
/**
 *
 * @author rivaldhy Irmwan
 * latihan 
 */
//public class BankOperations {
//        public BankOperations() {
//            if (!performSSNVerification()) {
//                throw new SecurityException("Access Denied!");
//            }
//        }
//
//        private boolean performSSNVerification() {
//            return false; // Returns true if data entered is valid, else false
//            // Assume that the attacker always enters an invalid SSN
//        }
//
//        public void greet() {
//            System.out.println("Welcome user! You may now use all the features.");
//        }
//    }
//
//    public class Storage {
//        private static BankOperations bop;
//
//        public static void store(BankOperations bo) {
//        // Store only if it is initialized
//            if (bop == null) {
//                if (bo == null) {
//                    System.out.println("Invalid object!");
//                    System.exit(1);
//                }
//                bop = bo;
//            }
//        }
//    }
//
//    public class UserApp {
//        public static void main(String[] args) {
//            BankOperations bo;
//            try {
//                bo = new BankOperations();
//            } catch (SecurityException ex) { bo = null; }
//
//            Storage.store(bo);
//            System.out.println("Proceed with normal logic");
//        }
//    }   

class BankOperations {
    private volatile boolean initialized = false;
    
    public BankOperations() {
        if (!performSSNVerification()) {
            return; // object construction failed
        }
        
        this.initialized = true; // Object construction successful
    }

    private boolean performSSNVerification() {
        return false;   
    }

    public void greet() {
        if (!this.initialized) {
            throw new SecurityException("Invalid SSN!");
    }

        System.out.println("Welcome user! You may now use all the features.");
    }
}
