/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author rivaldhy Irmwan
 * latihan 
 */
class CalendarSubclass extends Calendar {
    @Override public boolean after(Object when) {
    // Correctly calls Calendar.compareTo()
    if (when instanceof Calendar &&
        super.compareTo((Calendar) when) == 0) {
        return true;
    }
    return super.after(when);
    }
    
    @Override public int compareTo(Calendar anotherCalendar) {
    return compareDays(this.getFirstDayOfWeek(),
                       anotherCalendar.getFirstDayOfWeek());
    }

    private int compareDays(int currentFirstDayOfWeek,
                        int anotherFirstDayOfWeek) {
        return (currentFirstDayOfWeek > anotherFirstDayOfWeek) ? 1
        : (currentFirstDayOfWeek == anotherFirstDayOfWeek) ? 0 : -1;
    }
    
    public class ForwardingCalendar{
        private final CalendarImplementation c;
    
        public ForwardingCalendar(CalendarImplementation c) {
            this.c = c;
        }

        CalendarImplementation getCalendarImplementation() {
            return c;
        }

        public boolean after(Object when) {
            return c.after(when);
        }

        public int compareTo(Calendar anotherCalendar) {
        // CalendarImplementation.compareTo() will be called
        return c.compareTo(anotherCalendar);
        }
    }

    @Override
    protected void computeTime() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void computeFields() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void add(int field, int amount) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void roll(int field, boolean up) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getMinimum(int field) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getMaximum(int field) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getGreatestMinimum(int field) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getLeastMaximum(int field) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
    
