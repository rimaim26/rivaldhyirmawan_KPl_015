
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2;
/**
 *
 * @author rivaldhy Irmwan
 * latihan 
 */
public class CharCodePoint {
    
    public static String trim(String string) {
        char ch;
        int i;
        
        //uncompliant code
//        for (i=0;i<string.length();i++) {

        //compliant code
            for(i=0;i<string.length();i+=Character.charCount(ch)) {
            ch = string.charAt(i);
            if(!Character.isLetter(ch)) {
                break;
            }
        }
        return string.substring(i);
    }
    
    public static void main(String[] args) {
        String x = "example";
        trim(x);
    }
}
