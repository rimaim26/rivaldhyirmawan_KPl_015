/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2;

import java.io.*;

/**
 *
 * @author rivaldhy Irmwan
 * latihan 
 */
public class CompatibleCharJvm {
    
    static void jvm() {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream("SomeFile");
            DataInputStream dis = new DataInputStream(fis);
            byte[] data = new byte[1024];
            dis.readFully(data);
            //uncompliant code
            //String result = new String(data);
            
            //compliant code
            String result = new String(data, "UTF-16LE");
        } catch(IOException x) {
        //Handle error
        } finally {
            if(fis!=null) {
                try {
                    fis.close();
                } catch(IOException x) {
                    //Forward to handler
                }
            }
        }
    }
    
    public static void main(String[] args) {
        jvm();
    }
    
}
