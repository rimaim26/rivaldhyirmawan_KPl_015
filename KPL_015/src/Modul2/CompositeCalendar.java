/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author rivaldhy Irmwan
 * latihan 
 */
class CompositeCalendar extends CalendarSubclass.ForwardingCalendar {
    public CompositeCalendar(CalendarImplementation ci) {
    super(ci);
    }
    
    @Override public boolean after(Object when) {
    // This will call the overridden version, i.e.
    // CompositeClass.compareTo();
    if (when instanceof Calendar &&
    super.compareTo((Calendar)when) == 0) {
    // Return true if it is the first day of week
        return true;
    }
    // No longer compares with first day of week;
    // uses default comparison with epoch
    return super.after(when);
    }

    @Override public int compareTo(Calendar anotherCalendar) {
    return compareDays(
        super.getCalendarImplementation().getFirstDayOfWeek(),
        anotherCalendar.getFirstDayOfWeek());
    }
    
    private int compareDays(int currentFirstDayOfWeek,
                            int anotherFirstDayOfWeek) {
    return (currentFirstDayOfWeek > anotherFirstDayOfWeek) ? 1
            : (currentFirstDayOfWeek == anotherFirstDayOfWeek) ? 0 : -1;
    }

//    public static void main(String[] args) {
//        CalendarSubclass cs1 = new CalendarSubclass();
//        cs1.setTime(new Date());
//        // Date of last Sunday (before now)
//        cs1.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
//        // Wed Dec 31 19:00:00 EST 1969
//        CalendarSubclass cs2 = new CalendarSubclass();
//        // Expected to print true
//        System.out.println(cs1.after(cs2));
//    }
    
    public static void main(String[] args) {
        CalendarImplementation ci1 = new CalendarImplementation();
        ci1.setTime(new Date());
        // Date of last Sunday (before now)
        ci1.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        
        CalendarImplementation ci2 = new CalendarImplementation();
        CompositeCalendar c = new CompositeCalendar(ci1);
        // Expected to print true
        System.out.println(c.after(ci2));
    }

    // Implementation of other Calendar abstract methods
}
