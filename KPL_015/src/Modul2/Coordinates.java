/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2;

/**
 *
 * @author rivaldhy Irmwan
 * latihan 
 */
class Coordinates {
    private int x;
    private int y;

    public class Point {
        public void getPoint() {
            System.out.println("(" + x + "," + y + ")");
        }
    }
}

class AnotherClass {
    public static void main(String[] args) {
        Coordinates c = new Coordinates();
        Coordinates.Point p = c.new Point(); // Fails to compile
        p.getPoint();
    }
}
