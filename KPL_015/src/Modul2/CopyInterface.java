/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author rivaldhy Irmwan
 * latihan 
 */
public class CopyInterface {
    //java.util.Collection is an interface
    public void copyInterfaceInput(Collection<String> collection) {
        //doLogic(collection.clone());
        
        //compliant code
        collection = new ArrayList(collection);
        doLogic(collection);
    }

    private void doLogic(Collection<String> collection) {}
}
