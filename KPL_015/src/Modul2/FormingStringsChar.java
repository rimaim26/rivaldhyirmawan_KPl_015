/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

/**
 *
 * @author rivaldhy Irmwan
 * latihan 
 */

public class FormingStringsChar {
    
    public final int MAX_SIZE = 1024;
    
    public String readBytes(Socket socket) throws IOException {
    InputStream in = socket.getInputStream();
    byte[] data = new byte[MAX_SIZE+1];
    int offset = 0;
    int bytesRead = 0;
    String str = new String();
    while ((bytesRead = in.read(data, offset, data.length - offset)) != -1) {

        //uncompliant code
//        str += new String (data, offset, bytesRead, "UTF-8");
//        offset += bytesRead;

        //compliant code
        offset += bytesRead;
        if(offset >= data.length) {
            throw new IOException("Too much input");
        }
    }
    str += new String (data, 0, offset, "UTF-8");
    in.close();
    return str;
    }
    
    public static void main(String[] args) {
        
    }
}
