/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2;

import java.util.Date;
/**
 *
 * @author rivaldhy Irmwan
 * latihan 
 */
public final class MutableClass implements Cloneable {
    //uncompliant code
//    private Date date;
//
//    public MutableClass(Date d) {
//        this.date = d;
//    }
//
//    public void setDate(Date d) {
//        this.date = d;
//    }
//
//    public Date getDate() {
//        return date;
//    }
    
    //compliant solution (copy constructor)
//    private final Date date;
//    
//    public MutableClass(MutableClass mc) {
//        this.date = new Date(mc.date.getTime());
//    }
//
//    public MutableClass(Date d) {
//        this.date = new Date(d.getTime()); // Make defensive copy
//    }
//
//    public Date getDate() {
//        return (Date) date.clone(); // Copy and return
//    }
    
    //compliant solution (public static factory method)
//    private final Date date;
//    private MutableClass(Date d) { // Noninstantiable and nonsubclassable
//        this.date = new Date(d.getTime()); // Make defensive copy
//    }
//
//    public Date getDate() {
//        return (Date) date.clone(); // Copy and return
//    }
//
//    public static MutableClass getInstance(MutableClass mc) {
//        return new MutableClass(mc.getDate());
//    }
    
    //compliant solution (clone())
//    private final Date date; // final field
//    
//    public MutableClass(Date d) {
//        this.date = new Date(d.getTime()); // Copy in  
//    }
//    
//    public Date getDate() {
//        return (Date) date.clone(); // Copy and return
//    }
//
//    @Override
//    public Object clone() {
//        Date d = (Date) date.clone();
//        MutableClass cloned = new MutableClass(d);
//        return cloned;
//    }
    
    //compliant solution (clone() with final member)
    private final Date date; // final field
    
    public MutableClass(Date d) {
        this.date = new Date(d.getTime()); // Copy in
    }

    public Date getDate() {
        return (Date) date.clone(); // Copy and return
    }

    @Override
    public Object clone() {
        Date d = (Date) date.clone();
        MutableClass cloned = new MutableClass(d);
        return cloned;
    }
    
    public static void main(String[] args) {
        //main method
    }
}
