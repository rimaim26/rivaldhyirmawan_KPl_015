/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2;

import java.net.HttpCookie;

/**
 *
 * @author rivaldhy Irmwan
 * latihan 
 */
public final class MutableDemo {
    //java.net.HttpCookie is mutable
    public void useMutableInput(HttpCookie cookie) {
        if(cookie == null) {
            throw new NullPointerException();
        }
        
        //Create copy
        cookie = (HttpCookie)cookie.clone();
        
        //Check wether cookie has expired
        if(cookie.hasExpired()) {
            //Cookie is no longer valid; handle condition by throwing an exception
        }
        //doLogic(cookie);
        //Cookie may have expired since time of check
    }  
    
    public void deepCopy(int[] ints, HttpCookie[] cookies) {
        if(ints == null || cookies == null) {
            throw new NullPointerException();
        }
        
        //Shallow Copy
        int[] intsCopy = ints.clone();
        
        //Deep copy
        HttpCookie[] cookiesCopy = new HttpCookie[cookies.length];
        for(int i = 0; i < cookies.length; i++) {
            //Manually create copy of each element in array
            cookiesCopy[i] = (HttpCookie)cookies[i].clone();
        }
        
        doLogic(intsCopy, cookiesCopy);
    }

    private void doLogic(int[] intsCopy, HttpCookie[] cookiesCopy) {
        
    }
}
