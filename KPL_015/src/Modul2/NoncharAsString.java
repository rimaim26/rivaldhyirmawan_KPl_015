/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import java.math.BigInteger;

/**
 *
 * @author rivaldhy Irmwan
 * latihan 
 */
public class NoncharAsString {
    //uncompliant code
//    BigInteger x = new BigInteger("530500452766");
//    byte[] byteArray = x.toByteArray();
//    String s = new String(byteArray);
//    byteArray = s.getBytes();
//    x = new BigInteger(byteArray);
    
    //compliant code
//    BigInteger x = new BigInteger("530500452766");
//    String s = x.toString(); //valid char data
//    byte[] byteArray = s.getBytes();
//    String ns = new String(byteArray);
//    x = new BigInteger(ns);
    
        //compliant code (base64)
//        BigInteger x = new BigInteger("530500452766");
//        byte[] byteArray = x.toByteArray();
//        String s = Base64.getEncoder().encodeToString(byteArray);
//        byteArray = Base64.getEncoder().decode(s);
//        x = new BigInteger(byteArray);
    static void nonCharEncode() {
        //compliant code
        BigInteger x = new BigInteger("530500452766");
        String s = x.toString(); //valid char data
        byte[] byteArray = s.getBytes();
        String ns = new String(byteArray);
        x = new BigInteger(ns);
        System.out.println(x);
    } 
    
    public static void main(String[] args) {
        nonCharEncode();
    }

}
