/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2;

import java.util.Date;

/**
 *
 * @author rivaldhy Irmwan
 * latihan 
 */
class NotReturnRefToMutable {

//    private Date d;
//    
//    public NotReturnRefToMutable() {
//        d = new Date();
//    }
    //uncompliant code
//    public Date getDate() {
//        return d;
//    }
    
    //compliant solution
//    public Date getDate() {
//        return (Date)d.clone();
//    }
    
    private Date[] date;
    
    public NotReturnRefToMutable() {
    date = new Date[20];
        for(int i = 0; i < date.length; i++) {
            date[i] = new Date();
        }
    }

    public Date[] getDate() {
        Date[] dates = new Date[date.length];
        for (int i = 0; i < date.length; i++) {
        dates[i] = (Date) date[i].clone();
        }
    return dates;
    } 
}
