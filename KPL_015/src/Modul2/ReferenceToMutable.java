/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author rivaldhy Irmwan
 * latihan 
 */
public class ReferenceToMutable {
    //compliant solution 1
//    private static final SomeType [] SOMETHINGS = { ... };
//    public static final SomeType [] somethings() {
//        return SOMETHINGS.clone();
//    }   
    
    //compliant solution 2
    private static final SomeType [] THE_THINGS = { ... };
    public static final List<SomeType> SOMETHINGS =
    Collections.unmodifiableList(Arrays.asList(THE_THINGS));
}
