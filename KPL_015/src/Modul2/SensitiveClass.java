/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2;

/**
 *
 * @author rivaldhy Irmwan
 * latihan 
 */
class SensitiveClass {
    private char[] filename;
    private Boolean shared = false;
    
    public final SensitiveClass clone() 
            throws CloneNotSupportedException {
    throw new CloneNotSupportedException();
    }

    SensitiveClass(String filename) {
        this.filename = filename.toCharArray();
    }

    final void replace() {
        if (!shared) {
            for(int i = 0; i < filename.length; i++) {
            filename[i]= 'x' ;}
        }
    }
    
    final String get() {
        if (!shared) {
            shared = true;
            return String.valueOf(filename);
        } else {
            throw new IllegalStateException("Failed to get instance");
        }
    }

    final void printFilename() {    
        System.out.println(String.valueOf(filename));
    }
}
