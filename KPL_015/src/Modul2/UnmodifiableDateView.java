/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2;

import java.util.Date;

/**
 *
 * @author rivaldhy Irmwan
 * latihan 
 */
class UnmodifDateView extends Date {
    private Date date;

    public UnmodifDateView(Date date) {
        this.date = date;
    }

    public void setTime(long date) {
        throw new UnsupportedOperationException();
    }

    // Override all other mutator methods to throw UnsupportedOperationException
}

public final class UnmodifiableDateView {
        private Date date;
        public UnmodifiableDateView(Date d) {
            this.date = d;
        }

    public void setDate(Date d) {
        this.date = (Date) d.clone();
    }
    
    public UnmodifiableDateView getDate() {
        return new UnmodifiableDateView(date);
    }
    
    public static void main(String[] args) {
        //main method
    }
}
